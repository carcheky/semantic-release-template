#!/bin/bash


# set -eux

if [ ! -d node_modules ]; then
    npm ci
fi

counter_fix=0
counter_feat=0

counter() {
    if [[ "fix" = "${1}"  ]]; then
        echo fix
        counter_fix=$((counter_fix+1))
    elif [[ "feat" = "${1}"  ]]; then
        echo feat
        counter_feat=$((counter_feat+1))
    fi
}

# init & clean
init_clean() {
    git checkout dev
    git branch -D tmp_branch
    rm -fr CHANGELOG.md
    git tag -l | xargs -n 1 git push --delete origin -f
    git tag | xargs git tag -d
    git fetch --all --tags --prune
    git pull -f

    # first commit
    git checkout --orphan tmp_branch
    git add -A
    git commit --allow-empty -am "feat: Initial commit"

    # igualar ramas
    echo "==> borramos ramas"
    git branch -D dev
    git branch -D pre
    git branch -D pro
    echo "==> creamos dev"
    git checkout -b dev
    git branch -D tmp_branch
    git push --set-upstream origin dev -f
    git push origin dev -f
    git pull
    echo "==> creamos pre"
    git branch -D pre
    git checkout -b pre
    git push --set-upstream origin pre -f
    git push origin pre -f
    git pull
    echo "==> creamos pro"
    git branch -D pro
    git checkout -b pro
    git push --set-upstream origin pro -f
    git push origin pro -f
    git pull
    echo "==> empezamos el loop"
    full_loop
}

commit() {
    git commit --allow-empty -m "$1: $1/$1-$counter"
}

release() {
    echo copiando releaserc
    cp -fr .releaserc."$(git rev-parse --abbrev-ref HEAD)" .releaserc
    npx semantic-release
    git push
    git push --tags
}

goto_dev() {
    echo "
██████╗ ███████╗██╗   ██╗
██╔══██╗██╔════╝██║   ██║
██║  ██║█████╗  ██║   ██║
██║  ██║██╔══╝  ╚██╗ ██╔╝
██████╔╝███████╗ ╚████╔╝
╚═════╝ ╚══════╝  ╚═══╝

"
    git checkout dev
    git merge pre --no-ff --commit --no-edit
    git merge pro --no-ff --commit --no-edit
    git push
    release
}

goto_pre() {
    echo "
██████╗ ██████╗ ███████╗
██╔══██╗██╔══██╗██╔════╝
██████╔╝██████╔╝█████╗
██╔═══╝ ██╔══██╗██╔══╝
██║     ██║  ██║███████╗
╚═╝     ╚═╝  ╚═╝╚══════╝

"
    git checkout pre
    git merge dev --no-ff --commit --no-edit
    git merge pro --no-ff --commit --no-edit
    git push
    release
}

goto_main() {
    echo "
██████╗ ██████╗  ██████╗
██╔══██╗██╔══██╗██╔═══██╗
██████╔╝██████╔╝██║   ██║
██╔═══╝ ██╔══██╗██║   ██║
██║     ██║  ██║╚██████╔╝
╚═╝     ╚═╝  ╚═╝ ╚═════╝

"
    git checkout pro
    git merge pre --no-ff --commit --no-edit
    git merge dev --no-ff --commit --no-edit
    git push
    release
}

new() {
    echo "
███╗   ██╗███████╗██╗    ██╗
████╗  ██║██╔════╝██║    ██║
██╔██╗ ██║█████╗  ██║ █╗ ██║
██║╚██╗██║██╔══╝  ██║███╗██║
██║ ╚████║███████╗╚███╔███╔╝
╚═╝  ╚═══╝╚══════╝ ╚══╝╚══╝

    "
    counter "$1"
    declare -n counter="counter_$1"
    branchname="$1/$1-$counter"
    echo "===> $branchname"
    goto_dev
    git checkout -b "$branchname"
    commit "$1"
    git checkout dev
    git merge "$branchname" --no-ff --commit --no-edit
    git branch -D "$branchname"

}

full_loop() {
    goto_dev
    goto_pre
    goto_main
    goto_dev
}

full_loop
exit
# clean

init_clean
clear
new feat
new fix
new fix
new fix
full_loop
new fix
new fix
new fix
new fix
full_loop
new feat
new fix
new fix
new feat
new fix
full_loop
new fix
new fix
full_loop
new feat
full_loop
new feat
new fix
new fix
new feat
new fix
full_loop
new fix
full_loop
new fix
full_loop
