# CHANGELOG

# [1.5.0](https://gitlab.com/carcheky/semantic-release-template/compare/v1.4.2...v1.5.0) (2022-09-25)


### Features

* one pipeline 4 all ([680dbfd](https://gitlab.com/carcheky/semantic-release-template/commit/680dbfd60151cd08bef53bd8411af17f08273f6d))

## [1.4.2](https://gitlab.com/carcheky/semantic-release-template/compare/v1.4.1...v1.4.2) (2022-09-25)


### Bug Fixes

* fix/fix-17 ([628a956](https://gitlab.com/carcheky/semantic-release-template/commit/628a9564e67483ee44f36acfc3a135cf94ed8343))

## [1.4.1](https://gitlab.com/carcheky/semantic-release-template/compare/v1.4.0...v1.4.1) (2022-09-25)


### Bug Fixes

* fix/fix-16 ([b96ea2b](https://gitlab.com/carcheky/semantic-release-template/commit/b96ea2bc647c51d036ba4468128cbcc7f03a779e))

# [1.4.0](https://gitlab.com/carcheky/semantic-release-template/compare/v1.3.0...v1.4.0) (2022-09-25)


### Bug Fixes

* fix/fix-13 ([b0ebeb6](https://gitlab.com/carcheky/semantic-release-template/commit/b0ebeb6a4548d7941b88df7dbdb674abfeba2c72))
* fix/fix-14 ([530cbd6](https://gitlab.com/carcheky/semantic-release-template/commit/530cbd6323ef935549d90726e19e29015a50857f))
* fix/fix-15 ([36ddeef](https://gitlab.com/carcheky/semantic-release-template/commit/36ddeef979a3a107f2f55ef706dd74b73eaede79))


### Features

* feat/feat-5 ([8d747f9](https://gitlab.com/carcheky/semantic-release-template/commit/8d747f9d01630b95ba2e09dd083df681c688b848))
* feat/feat-6 ([d314230](https://gitlab.com/carcheky/semantic-release-template/commit/d314230ce57426d977d28ced27b5404d9e1fc828))

# [1.3.0](https://gitlab.com/carcheky/semantic-release-template/compare/v1.2.1...v1.3.0) (2022-09-25)


### Features

* feat/feat-4 ([bf82750](https://gitlab.com/carcheky/semantic-release-template/commit/bf82750f25127f8e5f12667f4aa25a6a8e09a47c))

## [1.2.1](https://gitlab.com/carcheky/semantic-release-template/compare/v1.2.0...v1.2.1) (2022-09-25)


### Bug Fixes

* fix/fix-11 ([cebec07](https://gitlab.com/carcheky/semantic-release-template/commit/cebec07ce8ab4899e72f2f7a3ca47be51d6a32cd))
* fix/fix-12 ([c40e270](https://gitlab.com/carcheky/semantic-release-template/commit/c40e270828be086ba8c599c6763957f265539aa9))

# [1.2.0](https://gitlab.com/carcheky/semantic-release-template/compare/v1.1.1...v1.2.0) (2022-09-25)


### Bug Fixes

* fix/fix-10 ([b558fc3](https://gitlab.com/carcheky/semantic-release-template/commit/b558fc34db2176a7438c960dbe09cbfe47d70331))
* fix/fix-8 ([ac8754e](https://gitlab.com/carcheky/semantic-release-template/commit/ac8754eb8f9dd83c21291f3d050ff88e7b53b529))
* fix/fix-9 ([24bce91](https://gitlab.com/carcheky/semantic-release-template/commit/24bce91a8be521479a1421ed4ae45ae605bdb9ad))


### Features

* feat/feat-2 ([1cd3e13](https://gitlab.com/carcheky/semantic-release-template/commit/1cd3e139fc346b08fd88905b11d686f3ce04e933))
* feat/feat-3 ([92565e4](https://gitlab.com/carcheky/semantic-release-template/commit/92565e47dd4b63d5d2341877186b7b9fd30ae8b5))

## [1.1.1](https://gitlab.com/carcheky/semantic-release-template/compare/v1.1.0...v1.1.1) (2022-09-25)


### Bug Fixes

* fix/fix-4 ([b692f1d](https://gitlab.com/carcheky/semantic-release-template/commit/b692f1d54be93d81542a285ff4be5f3e8f20d857))
* fix/fix-5 ([70606c3](https://gitlab.com/carcheky/semantic-release-template/commit/70606c376820495508206c09398c1882fe5bc560))
* fix/fix-6 ([20a7e43](https://gitlab.com/carcheky/semantic-release-template/commit/20a7e4324bf04f611461ad8c624ab487bac11e4f))
* fix/fix-7 ([43cbeb7](https://gitlab.com/carcheky/semantic-release-template/commit/43cbeb7a51c3903983341bee068e7cc59a4906d7))

# [1.1.0](https://gitlab.com/carcheky/semantic-release-template/compare/v1.0.0...v1.1.0) (2022-09-25)


### Bug Fixes

* fix/fix-1 ([18892ed](https://gitlab.com/carcheky/semantic-release-template/commit/18892ede162937bf6e6f305ed3c5bc3e2316a1a2))
* fix/fix-2 ([b0e592b](https://gitlab.com/carcheky/semantic-release-template/commit/b0e592be26af77a18a2f8c6c17c9eb02afa9b9ff))
* fix/fix-3 ([0a86d41](https://gitlab.com/carcheky/semantic-release-template/commit/0a86d412b2348fd31f74ed968e632412a8260795))


### Features

* feat/feat-1 ([7de358d](https://gitlab.com/carcheky/semantic-release-template/commit/7de358d441c94269a5202768b6ea2f3c5693724f))

# 1.0.0 (2022-09-25)


### Features

* Initial commit ([12a06a5](https://gitlab.com/carcheky/semantic-release-template/commit/12a06a5e6b9cd8fb63915d6b902d029faf4e085c))
