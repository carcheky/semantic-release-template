#!/bin/bash

sudo npm install --global git-conventional-commits
cp git-hooks/commit-msg .git/hooks -f
cp git-hooks/pre-receive .git/hooks -f

chmod +x .git/hooks/pre-receive
chmod +x .git/hooks/commit-msg