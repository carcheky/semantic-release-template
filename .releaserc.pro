{
  "branches": [
    '+([0-9])?(.{+([0-9]),x}).x',
    {name: 'pro', channel: 'pro'},
    {name: 'pre', channel: 'pre', prerelease: 'rc'},
    {name: 'dev', channel: 'dev', prerelease: 'beta'},
  ],
  "ci": false,
  "plugins": [
    "@semantic-release/commit-analyzer",
    "@semantic-release/release-notes-generator",
    [
      "@semantic-release/changelog",
      {
        "changelogTitle": "# CHANGELOG"
      }
    ],
    [
      '@semantic-release/git',
      {
      assets: [
        'CHANGELOG.md'
        ]
      }
    ]
  ]
}
